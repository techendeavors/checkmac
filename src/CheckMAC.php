<?php

namespace Techendeavors\CheckMAC;

use Illuminate\Support\Collection;

class CheckMAC
{
    private const API    = "https://api.macvendors.com/";

    public $vendor;

    public function __construct($mac)
    {
        $url = self::API.$mac;

        $curlHandle = curl_init($url);

        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 4);
        
        if (! $output = curl_exec($curlHandle)) {
            trigger_error(curl_error($curlHandle));
        };
        curl_close($curlHandle);

        $this->vendor = $output;
        //return($output);
    }

    public function __toString()
    {
        return $this->vendor;
    }
}
