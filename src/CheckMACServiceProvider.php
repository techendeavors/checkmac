<?php

namespace Techendeavors\CheckMAC;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\Container;

class CheckMACServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

        $this->app->alias('checkmac', CheckMAC::class);

        $this->app->singleton('checkmac', function (Container $app) {
            return new CheckMAC();
        });
    }
}
